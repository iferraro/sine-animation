import { Container } from "@mui/material";
import Header from "./Header";
import Graph from "./Graph";

function App() {
  return (
    <Container>
      <Header />
      <Graph />
    </Container>
  );
}

export default App;
