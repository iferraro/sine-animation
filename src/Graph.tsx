import React, { useEffect, useState } from "react";
import { Box, FormControlLabel, Switch, TextField } from "@mui/material";

const SEGMENTS = 1000;
const GRAPH_WIDTH = 1000;
const GRAPH_HEIGHT = 1000;
const GRAPH_FILL_COLOR = "#8CBBE9";
const LINE_COLOR = "#1976D2";
const LINE_WIDTH = 0.01;

interface Extremum {
  value: number;
  errorPresent: boolean;
}

interface Axis {
  [key: string]: Extremum;
  min: Extremum;
  max: Extremum;
}

// y1 = f(x) => original equation
// y2 = [f'(t)](x-t) + f(t) => tangent line of orig. equation at t

// dimensions on literal cartesian graph; width on screen will be set with SVG properties
const f = (x: number) => Math.sin(x);
// const fPrime = (x: number) => Math.cos(x);
const Graph = () => {
  const [frame, setFrame] = useState<Map<string, Axis>>(
    new Map([
      [
        "x",
        {
          min: {
            value: -2 * Math.PI,
            errorPresent: false,
          },
          max: {
            value: 2 * Math.PI,
            errorPresent: false,
          },
        },
      ],
      [
        "y",
        {
          min: {
            value: -2,
            errorPresent: false,
          },
          max: {
            value: 2,
            errorPresent: false,
          },
        },
      ],
    ])
  );

  // const [time, setTime] = useState<number>(0);
  const [errorPresent, setErrorPresent] = useState<boolean>(false);

  const handleFrameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    // Number("") === 0
    setFrame((currentFrame) => {
      const alteredKey = e.target.name[0]; // "x" or "y"
      const alteredAxis = currentFrame.get(alteredKey)!; // x or y objects
      const alteredExtremum = e.target.name.substring(1, 4); // "min" or "max"
      const newValue = Number(e.target.value); // number

      if (alteredExtremum === "min" && newValue >= alteredAxis.max.value) {
        // error on min
        alteredAxis.min.errorPresent = true;
      } else if (
        alteredExtremum === "max" &&
        newValue <= alteredAxis.min.value
      ) {
        // error on max
        alteredAxis.max.errorPresent = true;
      } else {
        // all good
        alteredAxis[alteredExtremum].value = newValue;
        alteredAxis[alteredExtremum].errorPresent = false;
      }
      return new Map(currentFrame).set(alteredKey, alteredAxis);
    });
  };

  const xMin = frame.get("x")!.min.value;
  const xMax = frame.get("x")!.max.value;
  const yMin = frame.get("y")!.min.value;
  const yMax = frame.get("y")!.max.value;

  const [showAxes, setShowAxes] = useState<boolean>(true);

  const handleSwitchChange = () => setShowAxes((showAxes) => !showAxes);

  const increment = (xMax - xMin) / SEGMENTS; // length of each segment

  const viewBox = `0 0 ${xMax - xMin} ${yMax - yMin}`;

  let fPath = `M 0,${yMax - f(xMin)}`;

  for (let x = xMin + increment; x <= xMax; x += increment) {
    fPath += ` L ${x - xMin},${yMax - f(x)}`;
  }

  // added one more line below (L <rightmost x-value>,<yMax - f(rightmost x-value)>) in case for loop cuts right side of graph short
  fPath += `
 L ${xMax - xMin},${yMax - f(xMax)} 
 V ${yMax - yMin} 
 H ${xMin - xMax} 
 Z`;

  const vertLineInFrame = xMin <= 0 && xMax >= 0;
  const horizLineInFrame = yMin <= 0 && yMax >= 0;

  const vertPath = `M ${-xMin},0 v ${yMax - yMin}`;
  const horizPath = `M 0,${yMax} h ${xMax - xMin}`;

  const generateHelperText = (relation: string, value: number) => {
    return `Must be ${relation} than ${value}`;
  };

  return (
    <Box>
      <FormControlLabel
        control={<Switch checked={showAxes} onChange={handleSwitchChange} />}
        label="Show Axes"
      />
      <TextField
        error={frame.get("x")?.min.errorPresent}
        type="number"
        variant="outlined"
        label="xMin"
        name="xmin"
        placeholder={String(xMin)}
        helperText={generateHelperText("less", frame.get("x")!.max.value)}
        onChange={handleFrameChange}
      />
      <TextField
        error={frame.get("x")?.max.errorPresent}
        type="number"
        variant="outlined"
        label="xMax"
        name="xmax"
        placeholder={String(xMax)}
        helperText={generateHelperText("greater", frame.get("x")!.min.value)}
        onChange={handleFrameChange}
      />
      <TextField
        error={frame.get("y")?.min.errorPresent}
        type="number"
        label="yMin"
        variant="outlined"
        name="ymin"
        placeholder={String(yMin)}
        helperText={generateHelperText("less", frame.get("y")!.max.value)}
        onChange={handleFrameChange}
      />
      <TextField
        error={frame.get("y")?.max.errorPresent}
        type="number"
        variant="outlined"
        label="yMax"
        name="ymax"
        placeholder={String(yMax)}
        helperText={generateHelperText("greater", frame.get("y")!.min.value)}
        onChange={handleFrameChange}
      />
      <svg width={GRAPH_WIDTH} /*height={GRAPH_HEIGHT}*/ viewBox={viewBox}>
        <path d={fPath} fill={GRAPH_FILL_COLOR} />
        {showAxes && vertLineInFrame && (
          <path d={vertPath} stroke={LINE_COLOR} strokeWidth={LINE_WIDTH} />
        )}
        {showAxes && horizLineInFrame && (
          <path d={horizPath} stroke={LINE_COLOR} strokeWidth={LINE_WIDTH} />
        )}
      </svg>
    </Box>
  );
};

export default Graph;
