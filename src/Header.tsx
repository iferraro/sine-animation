import { Box, Typography } from "@mui/material";

const Header = () => {
  return (
    <Box>
      <Typography>Sine Graph</Typography>
    </Box>
  );
};

export default Header;
